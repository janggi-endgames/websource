================
Cannon endgames
================

Cannon (漢包楚)
~~~~~~~~~~~~~~~

.. janggi-board::

    HK49,HC50,CK52

The cannon can only in the best case restrict the opposing king. This endgame is a draw.

Two cannons (漢包包楚)
~~~~~~~~~~~~~~~~~~~~~~

.. janggi-board::

    HK49,HC50,HC60,CK52

An extra cannon still cannot win as the second cannon lacks a mount. In fact there are no mating positions in this endgame.

Two cannons and a horse (漢包包馬楚)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The idea is executed in steps.

First put one cannon in the centre to avoid bigjang threats.

.. janggi-board::

    HK60,HC59,HC37,HH35,CK42

.. janggi-moves::

    1. HH35-27
    2. HC37-17
    3. HC17-47 

Secondly drive the opposing general away from the centre file with the other cannon and the horse.

.. janggi-moves::

    4. HK60-49
    5. HC59-09
    6. HH27-06
    7. HC09-02
    8. HH06-14
    9. HH14-22 check
    10. CK42-51

Since the general is denied access to the central file the cannon on the centre file can take control of it.

.. janggi-moves::

    11. HC47-50

Finally the horse can manoever to mate the king.

.. janggi-moves::

    12. HH22-34
    13. CK51-52
    14. HH34-13
    15. HH13-32 mate

.. janggi-board::

    HK49,HC50,HC02,HH32,CK52

Two cannons and an elephant (漢包包象楚)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Mating positions as the key position in the two cannons and horse endgame cannot be created
with an elepehant instead of the horse. A general in the centre will stay. If the centre is already in control Han can win as below.

.. janggi-board::

    CK52,HC26,HE27,HK49,HC50

.. janggi-moves::

    1. HC26-29
    2. HC29-59
    3. HE27-55 mate

Two cannons and a soldier (漢包包兵楚)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Two cannons and a soldier can win against a lone king. Here is an efficient way of mating:

.. janggi-board::

    CK42,HP45,HC39,HK49,HC58

.. janggi-moves::

    1. HK49-38
    2. HC58-28 
    3. HC28-48 check
    4. CK42-53
    5. HK38-49
    6. HC48-50
    7. CK53-42
    8. HP45-44
    9. HP44-43 check
    10. CK42-32 
    11. HP43-53
    12. HC39-59
    13. HC59-51
    14. HP53-42 mate 

.. janggi-board::

    HP42,HC51,CK32,HK49,HC50

Two cannons and an officer (漢包包士楚)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This endgame cannot in general be won. The exceptions are positions where the general and one cannon already are in control of the cetnre file.

.. janggi-board::

    CK52,HK49,HO38,HC28,HC50

.. janggi-moves::

    1. HC28-58
    2. HC58-40
    3. HC40-37
    4. HC37-39
    5. HC39-59
    6. HK49-48
    7. HO38-39 
    8. HC59-29
    9. HC29-49
    10. HO39-40
    11. HC50-30
    12. HO40-50
    13. HO50-60
    14. HC30-70
    15. HC70-50
    16. HO60-59
    17. HC49-69
    18. HO59-58
    19. HK48-59
    20. HC69-39
    21. HC39-59 mate

.. janggi-board::

    CK52,HC59,HK59,HO58,HC50

A cannon and a horse (漢包馬楚)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Mate positions exists, but cannot be enforced. For example:

.. janggi-board::

    HK40,HC50,HH57,CK43

.. janggi-moves::

    1. HH57-45 mate

The endgame is in general a draw. The addition of officers doesn't help.

A cannon and two horses (漢包馬馬楚)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The plan is to have one horse and the cannon restrict the opponent into one of the corners of the palace by placing the horse on 21, 61 or 44 and the cannon behind. Then the remaining horse can administer the mate.

.. janggi-board::

    CK42,HH27,HH57,HC48,HK49


.. janggi-moves::

    1. HH57-65
    2. HH65-44 check
    3. CK42-51
    4. HH27-08
    5. HH08-16
    6. HH16-24
    7. HH24-32 mate

.. janggi-board::

    CK51,HH32,HH44,HC48,HK49

Cannon, horse and elephant (漢包馬象楚)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This endgame is very similar to cannon and two horses. Restrict the general with two pieces and mate with the third.

.. janggi-board::

    CK32,HH74,HE18,HK49,HC59

.. janggi-moves::

    1. HE18-46
    2. CK32-42
    3. HH74-66
    4. HH66-45
    5. HE46-14 check
    6. CK42-43
    7. HC59-29
    8. HK49-39
    9. HC29-49 mate

.. janggi-board::

    CK43,HH45,HE14,HC49,HK39

Cannon, horse and soldier (漢包馬兵楚)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A horse and a solder can win against a lone general, but not if the soldier has reached the second or first rank. The addition of a cannon will turn endgames where the soldier is on the second rank into wins.

.. janggi-board::

    HP22,HH15,HC47,HK49,CK42

.. janggi-moves::

    1. HH15-36
    2. HH36-24
    3. HP22-32 check

If

.. janggi-moves::

    4. CK42-41
    5. HH24-43 mate

or if

.. janggi-moves::

    4. CK42-52
    5. HH24-45
    6. HP32-42 mate

.. janggi-moves::

    4. CK42-33
    5. HC47-50
    6. HP32-42
    7. HH24-12 mate

.. janggi-board::

    HH12,HP42,HC40,CK33,HK49

Cannon and elephant (漢包象楚)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

No mate exists. The endgame is a draw. The addition of officers does not help.

.. janggi-board::

    HK49,HC30,HE54,CK32

Cannon and two elephants (漢包象象楚)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This endgame is a challenge in maneuvering elephants on an open board.

.. janggi-board::

    CK33,HE15,HE76,HC78,HK49

.. janggi-moves::

    1. HE76-48
    2. CK33-42
    3. HC78-38
    4. HC38-60
    5. CK42-51
    6. HE15-32
    7. CK51-42
    8. HE32-55
    9. HE55-23
    10. CK42-53
    11. HE48-25 check
    12. CK53-43 big
    13. HE23-46
    14. HK49-59
    15. HE25-48
    16. CK43-42
    17. HE46-14 check
    18. CK42-41
    19. HE48-25
    20. HE25-8
    21. HE8-36
    22. HE36-64 mate

.. janggi-board::

    HE64,HE14,CK41,HK59,HC60

Cannon, elephant and soldier (漢包象兵楚)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The addition of a cannon to the elphant and soldier endgame will turn positions where the soldier has advanced to the second rank into wins.

.. janggi-board::

    HP22,HE15,HC47,HK49,CK42

.. janggi-moves::

    1. HP22-32 check
    2. CK42-33
    3. HC47-50
    4. HP32-22
    5. HE15-32
    6. HE32-64
    7. HE64-41
    8. HE41-73
    9. HE73-56 mate

.. janggi-board::

    HE56,HC50,HK49,CK33,HP22

Cannon and soldier (漢包兵楚)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Although there exists theoretical mate positions

.. janggi-board::

    HK49,HC50,HP42,CK31

these can next to never be enforced. Even given an ideal position a general on the side of the palace can not be stopped from going behind the soldier.

.. janggi-board::

    HK49,HC50,HP43,CK32

.. janggi-moves::

    1. HP43-32 check
    2. CK32-33

and no mate is possible. The addition of officers does not help.

Cannon and two soldiers (漢包兵兵楚)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The two soldiers endgame cannot be won if one soldier has advanced to the second rank or if both soldiers have advanced to the third rank. The addition of a cannon can turn these endgames into wins if one of the soldiers have advanced to the second rank.

.. janggi-board::

    CK42,HK38,HP34,HP22,HC8

Here the soldiers cannot help each other enter the palace, but the cannon can help.

.. janggi-moves::

    1. HC8-48
    2. HP34-44
    3. CK42-33 big
    4. HK38-49
    5. HC48-50 
    6. CK33-42
    7. HP44-43 check
    8. CK42-52
    9. HP22-32
    10. HP32-42 mate 
