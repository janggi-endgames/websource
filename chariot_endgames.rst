================
Chariot endgames
================

Chariot (漢車楚)
~~~~~~~~~~~~~~~~

.. janggi-board::

    HK39,HR33,CK52

The chariot can stop the opponent general from moving but it is impossible to mate as Cho can skip his turn. This endgame is always a draw.

Two chariots (漢車車楚)
~~~~~~~~~~~~~~~~~~~~~~~

The two chariot versus a lone general is an overwhelming win.

.. janggi-board::

    CK42,HR65,HR29,HK60

.. janggi-moves::

    1. HR29-49 check
    2. CK42-52 big
    3. CK65-55 mate


Two chariots versus chariot (漢車車楚車)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Mostly when it is Han to move the endgame can be won, but if it is Cho to move the outcome is mostly a draw. The strategy for Han is to check in every move.

.. janggi-board::

    CK51,CR42,HK39,HR9,HR20

.. janggi-moves::

    1. HR9-1 check
    2. CR42-41

Offering an exchange of chariots, which ofcoure Han doesn't want.

.. janggi-moves::

    3. HR20-60 check 
    4. CK51-42
    5. HR1-2 check
    6. CK42-33 big
    7. HR60-38 check
    8. CK33-43
    9. HR2-3 check
    10. CK43-42
    11. HR3-33 check
    12. CK42-52
    13. HR38-60 mate 

.. janggi-board::

    HR60,CK52,HR33,CR41,HK39

If instead Cho was to move in the same position the draw can be achieved by checking.

.. janggi-moves::

    1. CR42-32 check
    2. HK39-49

Han can never get of an infinite series of checks and the game is drawn.

Two chariots versus cannon (漢車車楚包)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Two chariots can win against cannon and general.

.. janggi-board::

    CK42,HR90,HR4,CC51,HK59

.. janggi-moves::

    1. HR4-44 check
    2. CK42-52 big
    3. HK59-49
    4. HR90-82 check
    5. CK52-53
    6. HR82-42 mate 

.. janggi-board::

    HR42,CK53,HR44,HK49

Two chariots versus horse (漢車車楚馬)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Two chariots can win against a horse.

.. janggi-board::

    HK39,CK42,HR7,HR6,CH32

.. janggi-moves::

    1. HR6-2
    2. HR7-37
    3. HR37-32 check
    4. CK42-51
    5. HR32-42 mate

.. janggi-board::

    HR42,CK51,HR2,HK39

Two chariots versus elephant (漢車車楚象)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

An elephant in defence cannot help cho against two chariots.

.. janggi-board::

    HK39,CK42,HR7,HR6,CE33

.. janggi-moves::

    1. HR7-37
    2. CE33-61
    3. HR6-3 
    4. HR37-33 check
    5. CK42-52
    6. HR33-53 mate 

.. janggi-board::

    HR53,CK52,HR3,CE61,HK39

Two chariots versus soldier (漢車車楚卒)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The two chariots can mostly ignore one soldier in defence and easily win.

.. janggi-board::

    HK39,CK42,HR17,HR86,CP35

.. janggi-moves::

    1. HR17-12 check
    2. CK42-41
    3. HR86-81 mate 

.. janggi-board::

    HK39,HR12,HR81,CK41,CP35

Two chariots versus officer (漢車車楚士)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

An officer is no match for the two chariots. This endgame is a win.

.. janggi-board::

    CK31,CO42,HR66,HR77,HK48

.. janggi-moves::

    1. HR66-61 check
    2. CK31-32
    3. HR77-37 check
    4. CO42-33
    5. HR61-51
    6. HR37-33 mate

.. janggi-board::

    HR33,HR51,CK32,HK48

Chariot and cannon (漢車包楚)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A chariot and a cannon versus a lone general will win by using
the chariot and general to help the cannon get behind the general to control the centre.

.. janggi-board::

    CK52,HR64,HC76,HK49

.. janggi-moves::

    1. HR64-66
    2. CK52-42 big
    3. HC76-46
    4. HR66-65
    5. HR65-45 check
    6. CK42-52
    7. HC46-50
    8. HR45-55 mate

.. janggi-board::

    CK52,HR55,HK49,HC50


Chariot and cannon vs chariot (漢車包楚車)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A chariot can defend against a chariot and a cannon. The chariot can simply defend the rank on which the king is positioned.

.. janggi-board::

    CK51,HK48,HC49,HR27,CR53

This position cannot be attacked and the game ends in a draw.

.. janggi-moves::

    1. HR27-21 check
    2. CK51-52

Chariot and cannon vs cannon (漢車包楚包)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A chariot and a cannon can win against a cannon.

.. janggi-board::

    CK42,CC41,HK40,HR14,HC75


.. janggi-moves::

    1. HR14-15
    2. CK42-31 big
    3. HR15-35 check
    4. CK31-42
    5. HR35-65
    6. HC75-45
    7. HK40-49
    8. HR65-62 check
    9. CK42-43 
    10. HR62-52
    11. HR52-51
    12. CC41-44
    13. HC45-50
    14. HR51-41 check
    15. CK43-33
    16. HR41-44
    17. HR44-42 mate


Chariot and horse (漢車馬楚)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Han can with either by restricting the general with the rook and mating with the horse or guarding the centre of the palace with the general and mating with the chariot. Both methods are demonstrated here.

.. janggi-board::

    CK51,HR74,HH66,HK49

.. janggi-moves::

    1. HR74-44
    2. HH66-54
    3. HR44-42 mate

.. janggi-board::

    CK51,HR42,HH54,HK49

.. janggi-board::

    CK52,HR33,HK50,HH90

.. janggi-moves::

    1. HH90-78
    2. HH78-57
    3. HH57-76
    4. HH76-64 mate

.. janggi-board::

    CK53,HR33,HK50,HH64

Chariot and horse vs soldier (漢車馬楚卒)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A chariot and a horse can win against a soldier.

.. janggi-board::

    HK39,CK42,CP57,HR38,HH5

.. janggi-moves::

    1. HH5-26
    2. CK42-41
    3. HR38-32
    4. CP57-58
    5. HH26-34
    6. CP58-49 check
    7. HK39-38
    8. HR32-42 mate

Han can mostly ignore the soldier. Note that capturing the soldier in move 7 would be bigjang.

.. janggi-board::

    HK38,HH34,HR42,CK41,CP49

Chariot and elephant (漢車象楚)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A chariot and an elephant win in the same way as chariot and horse versus lone general. For example

.. janggi-board::

    CK32,HR47,HK49,HE40

.. janggi-moves::

    1. HE40-8
    2. HE8-25
    3. HE47-42 mate

Chariot and soldier (漢車兵楚)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This endgame is a win if the soldier has not advanced to the back rank.

.. janggi-board::

    HP22,CK42,HR35,HK39

.. janggi-moves::

    1. HR35-32 check
    2. CK42-43 
    3. HR32-31
    4. HR31-51
    5. HP22-32
    6. HR51-42

.. janggi-board::

    HP32,HR42,HK39,CK43

Chariot and soldier versus chariot (漢車兵楚車)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A chariot can draw versus the chariot and soldier.
The draw can be achieved by letting the chariot defend across the board to stop
the soldier from advancing.

.. janggi-board::

    CK42,CR63,HP54,HR25,HK59

.. janggi-moves::

    1. HR25-22 check

not

.. janggi-moves::

    2. CK42-43
    3. HR22-23 check

and Cho will loose the chariot and the game. Instead

.. janggi-moves::

    2. CK42-41

and the game is a draw.

Chariot and soldier versus cannon (漢車兵楚包)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A cannon is no match for the chariot and soldier pair.

.. janggi-board::

    CC51,CK42,HP44,HR65,HK58

.. janggi-moves::

    1. HR65-63
    2. CK42-52 big
    3. HK58-49
    4. CK52-42
    5. HP44-43 check
    6. CK42-32
    7. HR63-53
    8. HP43-33 mate

.. janggi-board::

    HP33,HR53,CK32,CC51,HK49

Chariot and soldier versus horse (漢車兵楚馬)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A horse cannot stop the soldier from advancing. This endgame can be won by the chariot and soldier.

.. janggi-board::

    CK42,HP54,CH65,HR37,HK39

.. janggi-moves::

    1. HR37-35
    2. CH65-73
    3. HP54-44
    4. CH73-52
    5. HP44-34
    6. CK42-32
    7. HR35-25
    8. CK32-42
    9. HR25-23
    10. CK42-41 
    11. HR23-53
    12. CH52-73
    13. HP34-33
    14. CH73-61
    15. HR53-52
    16. HR52-62
    17. HR62-61 mate 

.. janggi-board::

    HR61,HP33,CK41,HK39

Cho should of course always look for forks that could save the draw. Sometimes
giving the drawing fork wouldneed more than one move. From the same position as above, but
with Cho to move.

.. janggi-moves::

    1. CH65-46
    2. HP54-44
    3. CH46-58

forking general and rook.

.. janggi-board::

    CH58,HK39,HR37,HP34,CK42

Chariot and soldier versus elephant (漢車兵楚象)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

As when the defender was a horse this endgame can be won by the combined chariot and soldier.

.. janggi-board::

    CK42,CE56,HP34,HR37,HK38

.. janggi-moves::

    1. HR37-36

Chasing ht elephant away from stopping the soldier.

.. janggi-moves::

    2. CE56-88
    3. HP34-33 check
    4. CK42-51
    5. HP33-43
    6. CK51-41
    7. HR36-32
    8. CE88-65
    9. HP43-53
    10. HR32-42 mate

.. janggi-board::

    HR42,HP53,CE65,CK41,HK38

Chariot and soldier versus soldier (漢車兵楚卒)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A soldier in defence cannot stop the chariot and soldier. If the Cho soldier blocks the path the chariot can easily force it to move away.

.. janggi-board::

    CK42,CP54,HP65,HR67,HK59

.. janggi-moves::

    1. HR67-57
    2. CP54-44
    3. HP65-64
    4. HP64-63
    5. HP63-53 check
    6. CK42-31
    7. HR57-67
    8. HR67-62
    9. HR62-42 mate 

.. janggi-board::

    CK31,HR42,HP53,CP44,HK59

Chariot and soldier versus two soldiers (漢車兵楚卒卒)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Two soldiers in defence cannot stop the chariot and soldier.

.. janggi-board::

    HK39,HP36,HR77,CK52,CP34,CP44

.. janggi-moves::

    1. HP36-26 
    2. HP26-25
    3. CK52-42
    4. HR77-27
    5. HP25-15
    6. CK42-32
    7. HP15-14
    8. CP34-24 big
    9. HR27-27 check 
    10. CP24-34
    11. HP14-13
    12. HP13-23 
    13. HR37-17
    14. CP34-24 big
    15. HK39-49
    16. CK32-42
    17. HR17-12 check
    18. CK42-43
    19. HR12-32
    20. CP44-34 big
    21. HK49-39 
    22. CK43-53
    23. HP23-33
    24. HR32-42 mate 

.. janggi-board::

    HK39,HP33,HR42,CK53,CP24,CP34

Chariot and soldier versus officer (漢車兵楚士)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

An officer can mostly draw against a chariot and a soldier.
It is important to keep the officer in front of the king. Careful play by the defender is required. The position below is drawn.

.. janggi-board::

    CK41,CO42,HP54,HR55,HK59

.. janggi-moves::

    1. HK55-65

Han threatens

.. janggi-moves::

    2. HR65-61 check
    3. CO42-51
    4. HP54-53
    5. HP53-52
    6. CK41-31
    7. HR61-51 check
    8. CK31-32
    9. HR51-41 mate

Cho must not let the officer block the king from moving out of check.

.. janggi-moves::

    2. CK41-31
    3. HR65-61 check

The task of the officer is to keep the soldier from entering the castle so using
it to protect the general in this position is a loosing idea.

.. janggi-moves::

    4. CO42-41 
    5. HP54-53
    6. HP53-43
    7. HP43-33
    8. HR61-62 
    9. CO41-51
    10. HR62-22
    11. HR22-21 mate

instead

.. janggi-moves::

    4. CK31-32 
    5. HR61-62
    6. CK32-31

keeps the soldier from entering the palace and saves the draw.

.. janggi-board::

    CK31,HR62,CO42,HP54,HK59

Chariot and two soldiers (漢車兵兵楚)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A chariot and two soldiers on the back rank cannot force a mate.

.. janggi-board::

    HP21,HP61,CK42,HR34,HK39
