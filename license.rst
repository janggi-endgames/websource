Copyright and licenses
======================

The janggi endgames webpage is Copyright Rikard Nordgren 2021 and licensed under the CC-BY-SA 4.0 license.

The janggi board graphics is licensed under the CC-BY-4.0 license. See https://github.com/Ka-hu/chess-pieces
