===========================
Longest immobility problems
===========================

Records
~~~~~~~

.. list-table::

    * - n pieces
      - endgame
      - longest mate
    * - 2
      - NA
      - No mate
    * - 3
      - NA
      - No mate
    * - 4 (so far)
      - :ref:`漢車兵楚`
      - 9
    * - 5 (so far)
      - :ref:`漢包馬楚包`
      - 5
    * - 6 (so far)
      - :ref:`漢包馬士楚包`
      - 16

All endgames
~~~~~~~~~~~~

.. list-table::

    * - Endgame
      - 漢
      - 楚
    * - :ref:`漢車兵楚`
      - 9
      - NA
    * - :ref:`漢包馬楚包`
      - 5
      - NA
    * - :ref:`漢包馬士楚包`
      - 16
      - NA
    * - :ref:`漢包象士楚包`
      - 15
      - NA


.. _漢車兵楚:

漢車兵楚
~~~~~~~~

.. janggi-board::

    CK32,HR53,HP87,HK48

.. janggi-moves::

    1. HP87-77
    2. HP77-67
    3. HP67-57
    4. HP57-47
    5. HP47-46
    6. HP46-45
    7. HP45-44
    8. HP44-43
    9. HP43-42 mate

.. _漢包馬楚包:

漢包馬楚包
~~~~~~~~~~

.. janggi-board::

    CK31,CC32,HK48,HC49,HH59

.. janggi-moves::

    1. HH59-67
    2. HH67-55
    3. HH55-63
    4. HH63-71
    5. HH71-52 mate


.. _漢包馬士楚包:

漢包馬士楚包
~~~~~~~~~~~~

.. janggi-board::

    HH51,CC42,CK33,HC34,HK38,HO39

.. janggi-moves::

    1. HO39-40
    2. HK38-39
    3. HO40-50
    4. HK39-40
    5. HO50-60
    6. HK40-50
    7. HO60-59
    8. HK50-60
    9. HC34-32
    10. HC32-40
    11. HC40-70
    12. HC70-60
    13. HO59-49
    14. HO49-39
    15. HC50-70
    16. HC70-40 mate


.. _漢包象士楚包:

漢包象士楚包
~~~~~~~~~~~~

.. janggi-board::

    HO39,HK38,HC36,HE15,CK33,CC42

.. janggi-moves::

    1. HO39-40
    2. HO40-50
    3. HO50-60
    4. HK38-39
    5. HK39-40
    6. HK40-50
    7. HC36-32
    8. HC32-35 
    9. HC35-5
    10. HC5-45
    11. HO60-49
    12. HO49-38
    13. HC45-5
    14. HC5-35
    15. HC35-39 mate
