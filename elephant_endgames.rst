=================
Elephant endgames
=================

One elephant (漢象楚)
~~~~~~~~~~~~~~~~~~~~~

An elephant can like the horse at most influence two points in the palace making this endgame a draw.

.. janggi-board::

    CK52,HE25,HK49

Two elephants (漢象象楚)
~~~~~~~~~~~~~~~~~~~~~~~~

.. janggi-board::

    CK51,HE24,HE74,HK49

Although there exists theoretical mate positions, like the one above, the opponent cannot be forced into any of these. In generanl this endgame is a draw.

Elephant and soldier (漢象兵楚) 
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. janggi-board::

    CK42,HP44,HE38,HK49

.. janggi-moves::

    1. HE38-66
    2. HP44-43 check

The elephant helps the soldier to enter the castle.

.. janggi-moves::

    3. CK42-32 
    4. HE66-89
    5. HE89-57 
    6. HE57-25
    7. HE25-48

This is the key position for the elephant. Regardless of the position of the opposing general Han can win in two moves.

.. janggi-moves::

    8. HE48-65
    9. HP43-42 mate

.. janggi-board::
    
    HP42,HE65,CK32,HK49

Two elephants and soldier (漢象象兵楚) 
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The addition of an elephant turns positions where the soldier has advanced to the second
rank into wins.

.. janggi-board::

    K42,HE37,HE45,HP22,HK49

.. janggi-moves::

    1. HE37-65 check
    2. CK42-53
    3. HP22-32
    4. CK53-43
    5. HK49-40
    6. HE45-77
    7. HE77-49
    8. HE49-26 check
    9. CK43-53
    10. HE65-48
    11. HE48-16
    12. HE26-58
    13. HE58-75
    14. HE16-48
    15. HE48-25 mate

.. janggi-board::

    HE25,HE75,CK53,HP32,HK40

Elephant and two soldiers (漢象兵兵楚) 
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The additional soldier for Han doesn't allow any wins that elephant and solder could already handle. For example

.. janggi-board::

    CK42,HH90,HP23,HP62,HK49

is a draw

Elephant and soldier versus chariot (漢象兵楚車) 
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A chariot can draw against elephant and soldier.

.. janggi-board::

    CK42,HK60,HP54,HE76,CR13

The chariot stops the soldier from advancing.


Elephant and soldier versus cannon (漢象兵楚包)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. janggi-board::

    CC31,CK42,HP54,HE75,HK60

.. janggi-moves::

    1. HP54-44
    2. CK42-51 big
    3. HK60-49 
    4. CC31-61
    5. HP44-43
    6. CC61-41 check
    7. HP43-53
    8. CC41-61 
    9. HE75-43
    10. CK51-41
    11. HK49-59 
    12. HE43-26 
    13. HE26-9 
    14. CC61-31
    15. HE9-37 
    16. CC31-51 check
    17. HP53-43
    18. CC51-31
    19. HE37-14
    20. CK41-51 big 
    21. HP43-53
    22. CC31-61
    23. HE14-46
    24. HE46-74 check
    25. CK51-41
    26. HP53-42 mate


Elephant and soldier versus elephant (漢象兵楚象)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This endgame is mostly drawn, but corrent play by the defender is required.
Draw can be achieved either by defending the centre of the palace or by blocking
the attacking elephant. Some similar positions with different strategies required will be shown
starting with a position where guarding the palace centre is the drawing strategy.

.. janggi-board::

    CK42,HP34,HE16,CE78,HK39

.. janggi-moves::

    1. HP34-33 check

not

.. janggi-moves::

    2. CK42-53
    3. HE16-48
    4. HE48-25 check
    5. CK53-52
    6. HP33-42 mate

Cho lost a crucial tempo to bring the elephant to protect the palace centre in time. Instead

.. janggi-moves::

    2. CK42-52
    3. HE16-48
    4. CE78-46
    5. HE48-25
    6. CE76-74 

and cho gets the defence up in time. The position is a draw.

.. janggi-board::

    CK52,HP33,CE74,HE25,HK38

If we alter the starting position slightly by moving the guarding elephant the strategy of blocking the attacking elephant is now needed to draw.

.. janggi-board::

    CK42,HP34,HE16,CE75,HK39

.. janggi-moves::

    1. HP34-33 check
    2. CK42-52
    3. HE16-48
    4. CE75-47

Now the Han elephant is blocked from advancing to the crucial 25 point and the game is a draw.

.. janggi-board::

    CE47,HE48,CK52,HP33,HK39

.. janggi-board::

    CK42,HP34,CE47,HE48,HK39

Knowing that the attacking elepphant in this case has to reach to 25 point gives the defender
a chance to calculate where to block.

.. janggi-moves::

    1. HE48-16
    2. CE47-15

Elephant and soldier versus elephant (漢象兵楚卒)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

An elephant and a soldier can win against a soldier. Quite many moves will ususally be needed to win.

.. janggi-board::

    CK42,CP44,HP35,HE58,HK39

.. janggi-moves::

    1. HE58-90
    2. HE90-67
    3. CP44-54
    4. HP35-45

not

.. janggi-moves::

    4. HP35-34

.. janggi-moves::

    5. HK39-49
    6. HE67-35
    7. HE35-58
    8. HE58-86
    9. CP54-64
    10. HK49-60
    11. HP45-55
    12. HE86-58
    13. HE58-75
    14. HE75-47
    15. HE47-70
    16. HE70-87
    17. CP64-74 
    18. HP55-54
    19. HE87-59
    20. CP74-75 
    21. HE59-36
    22. HP54-53 check
    23. CK42-33
    24. HE36-4
    25. CP75-65
    26. HE4-27
    27. CK33-32
    28. HP53-43
    29. CK32-31
    30. HE27-44
    31. CP65-55
    32. HE44-16
    33. CP55-45
    34. HK60-49
    35. HP43-33
    36. CP45-46
    37. HE16-48
    38. CP46-47
    39. HE48-76
    40. CP47-57
    41. HE76-53
    42. CK31-41
    43. HK49-39
    44. CK41-31
    45. HK39-40
    46. HE53-25
    47. CP57-58
    48. HK40-50
    49. CP58-49 check
    50. HK50-49
    51. HP33-42 mate 

Elephant and soldier versus officer (漢象兵楚士)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

As is the case for horse and soldier versus officer this endgame is a draw.

.. janggi-board::

    CK31,CO42,HP44,HE68,HK60

.. janggi-moves::

    1. HE68-85
    2. HP44-54
