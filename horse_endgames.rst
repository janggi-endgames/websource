==============
Horse endgames
==============


One horse (漢馬楚)
~~~~~~~~~~~~~~~~~~

A horse can at most influence two points in the palace at a time and is unable to mate on its own.

.. janggi-board::

    CK42,HH24,HK40

Two horses (漢馬馬楚)
~~~~~~~~~~~~~~~~~~~~~

There exists some theoretical mating positions like

.. janggi-board::

    CK51,HH33,HH63,CK39

but Cho cannot be forced into any of these so the endgame is in general a draw.

Two horses and an elephant (漢馬馬象楚)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. janggi-board::

    CK42,HH37,HH47,HE57,HK49


Use two of the pieces to restrict the king to one point. Then mate with the remaining piece.

.. janggi-moves::

    1. HE57-74 check
    2. CK42-41
    3. HH37-45
    4. HH45-24
    5. HH24-43
    6. HH47-26
    7. HH26-14
    8. HH14-22 mate

Two horses and a soldier (漢馬馬兵楚)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A horse and a soldier cannot win if the soldier is on the second rank. The addition of a horse
will turn such positions into wins. A soldier on the back rank can still not force a win together with two horses.

.. janggi-board::

    HP22,CK42,HH25,HH36,HK39

.. janggi-moves::

    1. HH25-13
    2. HH13-34 check
    3. CK42-33
    4. HH36-57 
    5. HH57-45 check
    6. CK33-43 
    7. HP22-32 
    8. HH34-55 mate 

.. janggi-board::

    HH55,HH45,JP32,CK43,HK39

Horse and elephant (漢馬象楚)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This endgame also have some theoretical mates that can be avoided. For example:

.. janggi-board::

    CK33,HH54,HE25,HK59

Horse, elephant and soldier (漢馬象兵楚)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A horse and soldier or elephant and soldier can win except when the soldier has advanced to the second or first rank. The addition of a horse or an elephant will allow Han to win even in positions where the soldier is on the second rank.

.. janggi-board::

    CK42,HE37,HH45,HP22,HK49

.. janggi-moves::

    1. HE37-65 check
    2. CK42-31
    3. HH45-33
    4. HP22-21 check 
    5. CK31-32 
    6. HH33-12
    7. HH12-24

.. janggi-board::

    HE65,CK31,HH24,HP21

Horse and two elephants (漢馬象象楚)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. janggi-board::

    CK42,HE37,HH47,HE57,HK49

.. janggi-moves::

    1. HE57-74 check 
    2. CK42-43
    3. HE37-5
    4. HK49-40
    5. HH47-66
    6. HK40-39
    7. HH66-45
    8. HE5-37
    9. HE37-9
    10 HE9-26 mate

.. janggi-board::

    HE26,HH45,HK39,CK42,HE74

Horse and soldier (漢馬兵楚)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A horse and a solder will together mate a lone king.

.. janggi-board::

    CK32,HH06,HP74,HK49

.. janggi-moves::

    1. HH6-27
    2. CK32-42 big
    3. HH27-46

The horse ends the bigjang threat.

.. janggi-moves::

    4. HP74-64
    5. HP64-54 
    6. HK49-60

To free the horse

.. janggi-moves::

    7. HH46-34 check
    8. CK42-43
    9. HP54-53 check
    10. CK43-33
    11. HH34-13
    12. HH13-21 check
    13. CK33-32
    14. HP53-42 mate

.. janggi-board::

    HP42,CK32,HH21,HK60

The horse both guards the soldier and stops the general from escaping behind the soldier. If the soldier has advanced to the back rank the position cannot be won and if the soldier is on the  second rank the position cannot in general be won as the king can go behind the soldier. For example:

.. janggi-board::

    HP22,CK42,HH44,HK49

.. janggi-moves::

    1. HP22-32 check
    2. CK42-33

The horse cannot move or else the soldier will be lost, nor can the soldier move into the centre of the caslte. A soldier on the third rank can win:

.. janggi-board::

    HH15,HP23,CK42,HK39

.. janggi-moves::

    1. HH15-36
    2. CK42-32
    3. HK39-49
    4. CK32-42 big
    5. HH36-44
    6. HK49-39
    7. HH44-25
    8. HP23-33 check
    9. CK42-53
    10. HH25-13
    11. HH13-34 check
    12. HP33-42 mate

.. janggi-board::

    HP42,HH34,CK53,HK39


Horse and soldier versus chariot (漢馬兵楚車)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A chariot can easily draw versus one horse and a soldier. The chariot can be sacrificed for one piece or engage in perpetual checking.

.. janggi-board::

    HP34,CK42,HH25,CR71,HK39

.. janggi-moves::

    1. HP34-33 check
    2. CK42-41
    3. HH25-44
    4. HH44-63
    5. CR71-51

or

.. janggi-moves::

    5. CR71-79 check

Horse and soldier versus cannon (漢馬兵楚包)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The cannon is unable to defend against horse and soldier.

.. janggi-board::

    CC31,CK42,HP54,HH65,HK60

As the cannon is stopping the soldier from directly entering the castle some other way of entry
has to be found.

.. janggi-moves::

    1. HP54-44
    2. CC31-53

Delaying the maneuvering of the horse

.. janggi-moves::

    3. HH65-73
    4. CC53-31 
    5. HH73-85 
    6. CK42-52 big 
    7. HK60-49 
    8. CK52-42
    9. HH85-64
    10. HP44-43 check
    11. CK42-51
    12. HH64-45 
    13. CC31-61
    14. HH45-26
    15. CC61-41 check
    16. HK49-40
    17. CC41-61
    18. HH26-34
    19. CC61-31 check
    20. HK40-49
    21. HP43-42 mate 

.. janggi-board::

    HP42,HK49,CC31,HH34,CK51

Horse and soldier versus horse (漢馬兵楚馬)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A horse can defend against a horse and a soldier.

.. janggi-board::

    CK51,HK40,HP53,CH21,HH54

The horse stops the soldier from going to the centre of the palace.


Horse and soldier versus elephant (漢馬兵楚象)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

An elephant can defend against a horse and a soldier.

.. janggi-board::

    CK51,HK40,HP53,HH56,CE14

.. janggi-moves::

    1. HH56-35
    2. CE14-37 
    3. HH35-23
    4. CE37-14

The elephant can stop the soldier from advancing to the centre of the palace

Horse and soldier versus soldier (漢馬兵楚卒)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A soldier in defence cannot stop Han from winning.

.. janggi-board::

    CK42,CP44,HP35,HH58,HK39

.. janggi-moves::

    1. HH58-37
    2. HH37-56
    3. CP44-54
    4. HP35-34 
    5. HH56-35
    6. CP54-64
    7. HP34-44
    8. HP44-43 check
    9. CK42-52
    10. HH35-23
    11. HH23-11
    12. CK52-51
    13. HP43-53
    14. HH11-23
    15. HP53-42 mate

.. janggi-board::

    HP42,HH23,CK51,CP64,HK39

Horse and soldier versus officer (漢馬兵楚士)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

An officer in defence is solid against the horse and soldier. This endgame is a draw.

.. janggi-board::

    CK41,CO42,HP54,HH65,HK60

The soldier cannot enter the palace without being captured by the officer.

Horse and two soldiers (漢馬兵兵楚)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A horse and a soldier cannot win if the soldier has advanced to the second rank. The addition of an extra second rank soldier doesn't create a win for Han.

.. janggi-board::

    CK42,HK38,HP62,HP12,HH37
