import os

from docutils import nodes
from docutils.parsers.rst import Directive

from sphinx.locale import _


class janggi_board(nodes.General, nodes.Element):
    pass


class janggi_moves(nodes.General, nodes.Element):
    pass


class JanggiBoardDirective(Directive):
    has_content = True

    def run(self):
        code = "\n".join(self.content)

        node = janggi_board()
        node["code"] = code
        result = [node]
        return result


class JanggiMovesDirective(Directive):
    has_content = True

    def run(self):
        node = janggi_moves()
        node['code'] = self.content
        result = [node]
        return result


def html_visit_janggi_board(self, node):
    from pyjanggi import Board
    board = Board(node['code'])
    svg = board._repr_svg_()
    svg = svg.replace('svg/', '_static/')
    self.body.append(f'<center>{svg}</center>')
    raise nodes.SkipNode


def html_visit_janggi_moves(self, node):
    lines = []
    for line in node['code']:
        a = line.split()
        move = a[1]
        if len(a) > 2:
            extra = a[2]
        else:
            extra = ''
        if move[0] == 'C':
            owner = '楚'
        else:
            owner = '漢'
        if move[1] == 'O':
            piece = '士'
        elif move[1] == 'E':
            piece = '象'
        elif move[1] == 'H':
            piece = '馬'
        elif move[1] == 'K':
            piece = ''
        elif move[1] == 'R':
            piece = '車'
        elif move[1] == 'C':
            piece = '包'
        elif move[1] == 'P':
            if move[0] == 'C':
                piece = '卒'
                owner = ''
            else:
                piece = '兵'
                owner = ''
        fromto = move[2:].split('-')
        if extra == 'big':
            extra = '빅'
        elif extra == 'check':
            extra = '장'
        elif extra == 'mate':
            extra = '승'
        else:
            extra = ''
        s = f'<tr style="border: none;"><td style="border: none;">{a[0]}</td><td style="border: none; text-align: right;">{fromto[0]}</td><td style="border: none; text-align: center;">{owner}{piece}</td><td style="border: none; text-align: right;">{fromto[1]}</td><td style="border: none; text-align: right;">{extra}</td></tr>'
        lines.append(s)
    html = ''.join(lines)
    start = f'<b><p><center><table style="border-collapse: collapse; border: none; font-family: batang, serif;"><col style="width: 3ch;"><col style="width: 3ch;"><col style="width: 5ch;"><col style="width: 1ch;"><col style="width: 5ch;">{html}</table></center></p></b>'
    self.body.append(start)
    raise nodes.SkipNode
 

def depart_janggi_board(self, node):
    return


def setup(app):
    setup.app = app
    setup.config = app.config
    setup.confdir = app.confdir

    app.add_directive("janggi-board", JanggiBoardDirective)
    app.add_directive("janggi-moves", JanggiMovesDirective)

    app.add_node(
        janggi_board,
        html=(html_visit_janggi_board, depart_janggi_board),
    )
    app.add_node(
        janggi_moves,
        html=(html_visit_janggi_moves, depart_janggi_board),
    )

    return {"version": "0.1"}
