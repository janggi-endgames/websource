Endgames in Janggi
==================

Welcome to the Janggi (Korean chess) endgames webpage.

This is an information resource on endgames in Janggi. It contains tutorials on how to play various endgames in Janggi, the longest possible endgames and the longest possible immobility problems for certain combinations of remaining pieces. The information is based on janggi endgame tablebases created by a program created by the author. Currently all endgames with 5-pieces have been solved and half of the endgames with 6-pieces. More information including the source code to the solver program will be released later.

News
~~~~

2021-12-18   Added all longest endgames with up to 5 pieces and many with 6 pieces

2021-12-07   First release of webpage

Rules
~~~~~

The rules used are the traditional janggi rules, i.e. the bigjang rule is used and the scoring rule is not used.

Contents
~~~~~~~~

.. toctree::
   :maxdepth: 1

   chariot_endgames
   cannon_endgames
   horse_endgames
   elephant_endgames
   soldier_endgames
   longest
   immobility
   license
